﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Db4objects.Db4o;

using Db4objects.Db4o.Linq;

namespace oo_datenbanken
{
    class Program
    {
        static void Main(string[] args)
        {
            using (IObjectContainer container = Db4oEmbedded.OpenFile("database.db4o"))
            {
				var result = from Person p in container select p;

				if(result.Count() == 0)
					FillDatabase(container);

                foreach (Person p in result)
                {
                    PrintParents(p);
                    Console.WriteLine("-----------------");
                }
                Console.ReadLine();
            }
        }

        static int recursionDepth = 0;

        static void PrintParents(Person p)
        {
            recursionDepth++;
            Console.WriteLine();
            Console.WriteLine(new string('\t', recursionDepth - 1) + p.vorname + ":");
            if (p.vater != null)
            {
                Console.WriteLine(new string('\t', recursionDepth-1) + "Vater: " + p.vater.vorname + " " + p.vater.nachname);
            }
            if (p.mutter != null)
            {
                Console.WriteLine(new string('\t', recursionDepth - 1) + "Mutter: " + p.mutter.vorname + " " + p.mutter.nachname);
            }
            if (p.vater != null)
            {
                PrintParents(p.vater);
            }
            if (p.mutter != null)
            {
                PrintParents(p.mutter);
            } 
            recursionDepth--;        
        }

        static void FillDatabase(IObjectContainer container) {
            int year = 2013;
            IDictionary<string, Person> stammbaum = new Dictionary<string, Person>();
            stammbaum.Add("George V", new Person("George V", "of England", year - 1865, null, null));
            stammbaum.Add("Mary", new Person("Mary", "of Teck", year - 1867, null, null));
            stammbaum.Add("Edward VIII", new Person("Edward VIII", "of England", year - 1894, stammbaum["George V"], stammbaum["Mary"]));
            stammbaum.Add("George VI", new Person("George VI", "of England", year - 1895, stammbaum["George V"], stammbaum["Mary"]));
            stammbaum.Add("Mary2", new Person("Mary", "of England", year - 1897, stammbaum["George V"], stammbaum["Mary"]));
            stammbaum.Add("Henry", new Person("Henry", "of England", year - 1900, stammbaum["George V"], stammbaum["Mary"]));
            stammbaum.Add("George", new Person("George", "of England", year - 1902, stammbaum["George V"], stammbaum["Mary"]));
            stammbaum.Add("John", new Person("John", "of England", year - 1905, stammbaum["George V"], stammbaum["Mary"]));
            stammbaum.Add("Elizabeth", new Person("Elizabeth", "Bowes Lyon", year - 1900, null, null));
            stammbaum.Add("Elizabeth II", new Person("Elizabeth II", "of England", year - 1926, stammbaum["George VI"], stammbaum["Elizabeth"]));
            stammbaum.Add("Margaret", new Person("Margaret", "of England", year - 1930, stammbaum["George VI"], stammbaum["Elizabeth"]));
            stammbaum.Add("Philip", new Person("Philip", "of Greece and Denmark", year - 1921, null, null));
            stammbaum.Add("Edward", new Person("Edward", "of England", year - 1964, stammbaum["Philip"], stammbaum["Elizabeth II"]));
            stammbaum.Add("Andrew", new Person("Andrew", "of England", year - 1960, stammbaum["Philip"], stammbaum["Elizabeth II"]));
            stammbaum.Add("Anne", new Person("Anne", "of England", year - 1950, stammbaum["Philip"], stammbaum["Elizabeth II"]));
            stammbaum.Add("Charles", new Person("Charles", "of England", year - 1948, stammbaum["Philip"], stammbaum["Elizabeth II"]));
            stammbaum.Add("Camilla", new Person("Camilla", "Parker Bowles", year - 1947, null, null));
            stammbaum.Add("William", new Person("William", "of England", year - 1982, stammbaum["Charles"], stammbaum["Camilla"]));
            stammbaum.Add("Diana", new Person("Diana", "of England", year - 1961, null, null));
            stammbaum.Add("Harry", new Person("Harry", "of England", year - 1984, stammbaum["Charles"], stammbaum["Diana"]));

            foreach (var person in stammbaum.Values)
            {
                container.Store(person);
            }
        }
    }
}