﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oo_datenbanken
{
    class Person
    {
        public string vorname;
        public string nachname;
        public int alter;
        public Person vater;
        public Person mutter;

        public Person(string vorname, string nachname, int alter, Person vater, Person mutter)
        {
            this.vorname = vorname;
            this.nachname = nachname;
            this.alter = alter;
            this.vater = vater;
            this.mutter = mutter;
        }
    }
}
